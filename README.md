# Jsinjection

## BROWSER: URL

```javascript
javascript:document.write("Thanks for you!!!");

javascript:document.alert("Thanks for you!!!");
```

## Input

```javascript
<script>...</script>

<script src="https://.../jquery.min.js"   >...</script>
```



## Get the content of a Class or Id

```javascript
getCode = document.getElementsByClassName("ace-syntax")[0]; 
console.log(getCode.textContent);


alert(document.getElementById("firstHeading").textContent); 
```


## Cookie - Parameters  Modification

During this injection attack, a malicious user can gain parameters information or change any parameters value (Example, cookie settings). This can cause quite serious risks as a malicious user can gain sensitive content. Such a type of injection can be performed using some Javascript commands.

Let’s remember, that the Javascript command returning the current session cookie is written accordingly:
```javascript
javascript: alert(document.cookie);
```

For Example, if we have found a vulnerable website, that stores session id in the cookie parameter ‘session_id‘. Then we can write a function, that changes the current session id:
```javascript
javascript:void(document.cookie=“session_id=<<other session id>>“);
```

For Example, a malicious user wants to log in as other people. To perform a login, the malicious user firstly will change authorization cookie settings to true. If cookie settings are not set as “true“, then the cookie value can be returned as “undefined“.
```javascript
javascript:void(document.cookie=“authorization=true“);
```

For Example, if a website‘s developer wasn‘t cautious enough, it can return username and password parameters names and values also. Then such information can be used for hacking the website or just changing the sensitive parameter’s value.
```javascript
javascript:void(document.cookie=”username=otherUser”); 
```


## Cambiar fondo de pantalla
document.body.style.backgroundColor = "blue";
document.bgColor="blue"; 


## Get Date
```javascript
Get fecha y hora actual
new Date()
```


## location
```javascript
location.href 
location.hostname
location.protocol
```
